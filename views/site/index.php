<?php

/* @var $this yii\web\View */
/* @var $list array */

$this->title = 'ToDo List';
?>
<div class="site-index">
    <script>
        var BaseUrl = '<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>';
        var listToDo = <?=\yii\helpers\Json::encode($list)?>;
    </script>

    <div class="body-content">

        <section class="todoapp">
            <header class="header">
                <h1>todos</h1>
                <input class="new-todo" placeholder="What needs to be done?" autofocus>
            </header>
            <!-- This section should be hidden by default and shown when there are todos -->
            <section class="main <?=!count($list) ?? 'hidden'?>">
                <input id="toggle-all" class="toggle-all" type="checkbox">
                <label for="toggle-all">Mark all as complete</label>
                <ul class="todo-list">
                </ul>
            </section>
            <!-- This footer should hidden by default and shown when there are todos -->
            <footer class="footer <?=!count($list) ?? 'hidden'?>">
                <span class="todo-count"><strong>0</strong> item left</span>
                <ul class="filters">
                    <li>
                        <a href="#/">All</a>
                    </li>
                    <li>
                        <a href="#/active">Active</a>
                    </li>
                    <li>
                        <a href="#/completed">Completed</a>
                    </li>
                </ul>
            </footer>
        </section>

    </div>
</div>
