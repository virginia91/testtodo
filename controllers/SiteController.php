<?php

namespace app\controllers;

use app\models\ActiveRecord\ListToDo;
use app\models\SignupForm;
use app\models\ActiveRecord\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $list = ListToDo::find()->where(['user_id' => Yii::$app->user->id])->orderBy('id')->asArray()->all();
        return $this->render('index', ['list' => $list]);
    }

    /**
     * Сохранение изменений
     *
     * @return array|Response
     */
    public function actionSave()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $data = \Yii::$app->request->post('data');
        if (isset($data['id']) && $data['id']) {
            $model = ListToDo::find()->where(['user_id' => Yii::$app->user->id, 'id' => $data['id']])->one();
            if ($model) {
                if (isset($data['status'])) {
                    $model->status = $data['status'];
                }
                if (isset($data['text'])) {
                    $model->text = $data['text'];
                }
                if ($model->validate()){
                    $model->save();
                    return ['success' => true];
                } else {
                    return ['success' => false, 'data' => $model->errors];
                }
            }
        } else {
            $model = new ListToDo();
            $model->text = $data['text'];
            $model->user_id = Yii::$app->user->id;
            if ($model->validate()){
                $model->save();
                return ['success' => true, 'data' => $model->id];
            } else {
                return ['success' => false];
            }
        }
    }

    /**
     * Удаление элемента
     *
     * @param $id
     * @return array|Response
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $item = ListToDo::find()->where(['user_id' => Yii::$app->user->id, 'id' => $id])->asArray()->one();

        if ($item) {
            $result = ListToDo::deleteAll(['id' => $id]);
            if ($result) {
                return ['success' => true];
            }
        }
        return ['success' => false];
    }

    /**
     * @return string|Response
     */
    public function actionSignup(){
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new SignupForm();
        if($model->load(\Yii::$app->request->post()) && $model->validate()){
            $user = new User();
            $user->login = $model->login;
            $user->password = \Yii::$app->security->generatePasswordHash($model->password);
            if($user->save()){
                return $this->goHome();
            }
        }
        return $this->render('signup', compact('model'));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
