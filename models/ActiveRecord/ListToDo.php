<?php

namespace app\models\ActiveRecord;

use yii\db\ActiveRecord;

class ListToDo extends ActiveRecord
{
    public static function tableName()
    {
        return '{{list}}';
    }

    public function rules()
    {
        return [
            [['text', 'user_id'], 'required'],
            ['status', 'boolean'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }




}
