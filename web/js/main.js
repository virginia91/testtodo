$(document).ready(function(){

    var body = $('body');
    var filter = window.location.hash;

    if (!filter) {
        $('ul.filters').find('a[href="#/"]').addClass('selected');
    } else {
        $('ul.filters').find('a[href="'+filter+'"]').addClass('selected');

    }

    $.each(listToDo, function (i, item) {
        view(item);
    });
    calculate();

    // сохранение изменений
    function save(data)
    {
        $.ajax({
            url: BaseUrl+"/site/save",
            type: "POST",
            data: {
                data: data
            },
            success: function(response) {
                if (response.success === true) {
                    if (response.data) {
                        data.id = response.data;
                        data.status = false;
                         view(data);

                    }
                    calculate();
                }
                // console.log(response)
            },
            error:function (response) {},
            dataType : 'json'
        });
    }

    // удаление
    function deleteItem(elem)
    {
        $.ajax({
            url: BaseUrl+"/site/delete?id="+elem.data('id'),
            type: "GET",
            success: function(response) {
                if (response.success === true) {
                    elem.remove();
                    calculate();
                }
            },
            error:function (response) {},
            dataType : 'json'
        });
    }

    // вывод
    function view(data)
    {
        var status = data['status'] ? "completed" : "";
        var checked = data['status'] ? "checked" : "";
        var flag = "";
        if ((filter === '#/completed' && !data['status']) || (filter === '#/active' && data['status'])) {
            flag = "hidden";
        }
        var item = '<li data-id="'+data['id']+'" class="'+status + ' '+ flag +'">' +
            '        <div class="view">\n' +
            '          <input class="toggle" type="checkbox" '+checked+'>\n' +
            '          <label class="text">'+ data['text'] +'</label>\n' +
            '          <button class="destroy"></button>\n' +
            '        </div>\n' +
            '        <input class="edit" value="'+ data['text'] +'">\n' +
            '       </li>';

        $('ul.todo-list').append(item);
    }

    function calculate()
    {
        var complete = false;
        var items = 0;
        var count = 0;
        $('ul.todo-list li').each(function() {
            count++;
            if ($(this).hasClass('completed')) {
                complete = true;
            } else {
                items++;
            }
        });

        if (count) {
            $('section.main, footer').removeClass('hidden');
        } else {
            $('section.main, footer').addClass('hidden');
        }

        var buttonComplete = $('footer').find('button.clear-completed');
        if (complete) {
            if(buttonComplete.length === 0) {
                $('footer').append('<button class="clear-completed">Clear completed</button>');
            }
        } else {
            buttonComplete.remove();
        }

        $('span.todo-count strong').html(items);
    }

    // добавление нового элемента
    $('.new-todo').keypress(function (e) {
        if (e.keyCode === 13) {
            save({text: $(this).val()});
            $(this).val('');
        }
    });

    // изменение статуса элемента
    body.on('change', 'input.toggle', function () {
        var parent = $(this).parents('li');
        if ($(this).prop('checked') === true) {
            save({id: parent.data('id'), status: 1});
            parent.addClass('completed');
        } else {
            save({id: parent.data('id'), status: 0});
            parent.removeClass('completed');
        }
    });

    // удаление элемента
    body.on('click', 'button.destroy', function () {
        var parent = $(this).parents('li');
        deleteItem(parent);
    });

    // изменение текста элемента
    body.on('dblclick', 'ul.todo-list li', function () {
        $(this).addClass('editing');
        var el = $(this).find('input.edit');
        var val = el.val();
        el.val('').focus().val(val);
    });

    // сохранение измененного текста
    body.on('focusout', 'ul.todo-list input.edit', function (e) {
        var parent = $(this).parents('li');
        save({text: $(this).val(), id: parent.data('id')});
        parent.find('label.text').html($(this).val());
        parent.removeClass('editing');
    });

    // удаление завершенных дел
    body.on('click', 'button.clear-completed', function () {
        $('ul.todo-list li').each(function(i,elem) {
            if ($(this).hasClass('completed')) {
                deleteItem($(this));
            }
        });
    });

    // отметить все как активные/завершенные
    $('.toggle-all').click(function () {
        var checked = $(this).prop('checked');
        $('ul.todo-list li').each(function(i,elem) {
            $(this).find('input.toggle').prop('checked', checked).change();

        });
    });

    // фильтр все
    $('a[href="#/active"]').click(function () {
        $(this).parents('ul.filters').find('a').removeClass('selected');
        $(this).addClass('selected');
        filter = '#/active';
        $('ul.todo-list li').each(function() {
            if ($(this).hasClass('completed')) {
                $(this).addClass('hidden');
            } else {
                $(this).removeClass('hidden');
            }
        });
    });

    // фильтр завершенные
    $('a[href="#/completed"]').click(function () {
        $(this).parents('ul.filters').find('a').removeClass('selected');
        $(this).addClass('selected');
        filter = '#/completed';
        $('ul.todo-list li').each(function() {
            if (!$(this).hasClass('completed')) {
                $(this).addClass('hidden');
            } else {
                $(this).removeClass('hidden');
            }
        });
    });

    // фильтр все
    $('a[href="#/"]').click(function () {
        $(this).parents('ul.filters').find('a').removeClass('selected');
        $(this).addClass('selected');
        filter = '';
        $('ul.todo-list li').each(function() {
            $(this).removeClass('hidden');
        });
    });
});