<?php

use yii\db\Migration;

/**
 * Class m181104_163006_create_table_list
 */
class m181104_163006_create_table_list extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \Yii::$app->db->getMasterPdo()->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
        \Yii::$app->db->getMasterPdo()->query('
            -- Table: list

            -- DROP TABLE list;
            
            CREATE TABLE list
            (
              id serial NOT NULL,
              status boolean DEFAULT false,
              text character varying(255) NOT NULL,
              user_id integer,
              CONSTRAINT "PK_list" PRIMARY KEY (id),
              CONSTRAINT "FK_list" FOREIGN KEY (user_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE NO ACTION ON DELETE NO ACTION
            )
            WITH (
              OIDS=FALSE
            );
                    
            -- Index: index_list
            
            -- DROP INDEX index_list;
            
            CREATE INDEX index_list
              ON list
              USING btree
              (user_id);
            
            -- Index: index_list_id
            
            -- DROP INDEX index_list_id;
            
            CREATE INDEX index_list_id
              ON list
              USING btree
              (id);
            ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181104_163006_create_table_list cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181104_163006_create_table_list cannot be reverted.\n";

        return false;
    }
    */
}
