<?php

use yii\db\Migration;

/**
 * Class m181104_162957_create_table_user
 */
class m181104_162957_create_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \Yii::$app->db->getMasterPdo()->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
        \Yii::$app->db->getMasterPdo()->query('
            -- Table: "user"

            -- DROP TABLE "user";
            
            CREATE TABLE "user"
            (
              id serial NOT NULL,
              login character varying(255) NOT NULL,
              password character varying NOT NULL,
              CONSTRAINT "PK_user" PRIMARY KEY (id),
              CONSTRAINT user_login UNIQUE (login)
            )
            WITH (
              OIDS=FALSE
            );
            ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181104_162957_create_table_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181104_162957_create_table_user cannot be reverted.\n";

        return false;
    }
    */
}
